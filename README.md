# Z270 PC Mate

OpenCore: 0.6.7

BIOS:

- WIP

Kexts:

- [VirtualSMC](https://github.com/acidanthera/VirtualSMC/releases) (**Core**)
- [Lilu](https://github.com/acidanthera/Lilu/releases) (**Core**)
- [WhateverGreen](https://github.com/acidanthera/WhateverGreen/releases) (Intel HD Graphics)
- [IntelMausi](https://github.com/acidanthera/IntelMausi/releases) (Ethernet)
- [AirportBrcmFixup](https://github.com/acidanthera/AirportBrcmFixup/releases) (Wifi)
- [BrcmPatchRAM](https://github.com/acidanthera/BrcmPatchRAM/releases) (Bluetooth)

Drivers:

- HFSPlus.efi
- OpenRuntime.efi

Tools:

- OpenShell.efi
